import React from "react";
import { Board } from "./modules/Board";

const App = () => <Board />;

export default App;
