export const CellType = Object.freeze({
  TEXT: Symbol("TEXT"),
  EMPTY: Symbol("EMPTY"),
  SHIP: Symbol("SHIP"),
  SHOOTED_EMPTY: Symbol("SHOOTED_EMPTY"),
  SHOOTED_SHIP: Symbol("SHOOTED_SHIP"),
});

export const cellCountInRow = 11;
export const cellCountInColumn = 11;
export const boardCellsCount = cellCountInRow * cellCountInColumn;

export const defaultMatrixCellsByIndex = {
  0: { type: CellType.TEXT, text: "" },
  1: { type: CellType.TEXT, text: "0" },
  2: { type: CellType.TEXT, text: "1" },
  3: { type: CellType.TEXT, text: "2" },
  4: { type: CellType.TEXT, text: "3" },
  5: { type: CellType.TEXT, text: "4" },
  6: { type: CellType.TEXT, text: "5" },
  7: { type: CellType.TEXT, text: "6" },
  8: { type: CellType.TEXT, text: "7" },
  9: { type: CellType.TEXT, text: "8" },
  10: { type: CellType.TEXT, text: "9" },
  11: { type: CellType.TEXT, text: "0" },
  22: { type: CellType.TEXT, text: "1" },
  33: { type: CellType.TEXT, text: "2" },
  44: { type: CellType.TEXT, text: "3" },
  55: { type: CellType.TEXT, text: "4" },
  66: { type: CellType.TEXT, text: "5" },
  77: { type: CellType.TEXT, text: "6" },
  88: { type: CellType.TEXT, text: "7" },
  99: { type: CellType.TEXT, text: "8" },
  110: { type: CellType.TEXT, text: "9" },
};

export const shipsLayout = [
  {
    ship: "carrier",
    positions: [
      [2, 9],
      [3, 9],
      [4, 9],
      [5, 9],
      [6, 9],
    ],
  },
  {
    ship: "battleship",
    positions: [
      [5, 2],
      [5, 3],
      [5, 4],
      [5, 5],
    ],
  },
  {
    ship: "cruiser",
    positions: [
      [8, 1],
      [8, 2],
      [8, 3],
    ],
  },
  {
    ship: "submarine",
    positions: [
      [3, 0],
      [3, 1],
      [3, 2],
    ],
  },
  {
    ship: "destroyer",
    positions: [
      [0, 0],
      [1, 0],
    ],
  },
];
