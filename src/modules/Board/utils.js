import {
  boardCellsCount,
  cellCountInRow,
  CellType,
  defaultMatrixCellsByIndex,
} from "./constants";

export const getCellIndexByCoordinates = (x, y) =>
  x + 1 + (y + 1) * cellCountInRow;

export const getMatrixCellsByIndex = (filledMatrixCellsByIndex) =>
  Array.from(Array(boardCellsCount)).map(
    (cell, index) =>
      defaultMatrixCellsByIndex[index] ||
      filledMatrixCellsByIndex[index] || { type: CellType.EMPTY }
  );

export const getShipIndexes = (ship) =>
  ship.positions.reduce(
    (indexes, [x, y]) => ({
      ...indexes,
      [getCellIndexByCoordinates(x, y)]: {
        type: CellType.SHIP,
        text: ship.ship,
      },
    }),
    {}
  );

export const getMatrixIndexesByLayout = (shipsLayout) =>
  shipsLayout.reduce(
    (result, ship) => ({ ...result, ...getShipIndexes(ship) }),
    {}
  );

export const isGameEnd = (matrixCellsByIndex = []) =>
  !matrixCellsByIndex.some((cell) => cell.type === CellType.SHIP);
