import React, { memo } from "react";
import { CellType } from "./constants";
import classNames from "classnames";

const Cell = ({ index, type, text = "", onShoot }) => (
  <div
    onClick={() => onShoot(index)}
    className={classNames(
      "w-7 h-7 sm:w-12 sm:h-12 md:w-16 md:h-16 border border-gray-700 grid items-center justify-items-center text-lg md:text-2xl",
      {
        "bg-blue-500 hover:bg-purple-700 cursor-pointer":
          type === CellType.EMPTY || type === CellType.SHIP,
      },
      {
        "bg-gray-50 pointer-events-none":
          type === CellType.TEXT || type === CellType.SHOOTED_EMPTY,
      },
      { "bg-red-500 pointer-events-none": type === CellType.SHOOTED_SHIP }
    )}
  >
    {type === CellType.TEXT ? text : ""}
  </div>
);

export default memo(Cell);
