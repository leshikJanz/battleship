import React, { useMemo, useState, useCallback } from "react";
import Cell from "./Cell";
import { CellType, shipsLayout } from "./constants";
import {
  getMatrixCellsByIndex,
  getMatrixIndexesByLayout,
  isGameEnd,
} from "./utils";

const Board = () => {
  const filledMatrixCellsByIndex = useMemo(
    () => getMatrixIndexesByLayout(shipsLayout),
    [shipsLayout]
  );

  const [matrixCellsByIndex, setMatrixCellsByIndex] = useState(
    getMatrixCellsByIndex(filledMatrixCellsByIndex)
  );

  const [gameEnd, setGameEnd] = useState(false);

  const checkGameEnd = (matrixCellsByIndex) => {
    if (!gameEnd && isGameEnd(matrixCellsByIndex)) {
      setGameEnd(true);
    }
  };

  const onShoot = useCallback((index) => {
    setMatrixCellsByIndex((prevMatrixCellsByIndex) => {
      const clonedMatrixCellsByIndex = [...prevMatrixCellsByIndex];

      clonedMatrixCellsByIndex[index] = {
        ...prevMatrixCellsByIndex[index],
        type:
          prevMatrixCellsByIndex[index].type === CellType.SHIP
            ? CellType.SHOOTED_SHIP
            : CellType.SHOOTED_EMPTY,
      };
      checkGameEnd(prevMatrixCellsByIndex);

      return clonedMatrixCellsByIndex;
    });
  }, []);

  return (
    <>
      <div className="grid grid-cols-11 w-max mx-auto mt-6">
        {matrixCellsByIndex.map((cell, index) => (
          <Cell
            key={index}
            index={index}
            type={cell.type}
            text={cell.text}
            onShoot={onShoot}
          />
        ))}
      </div>
      {gameEnd && (
        <h2 className="text-xl sm:text-2xl text-yellow-700 text-center pt-4">
          Game over! Nice shooting!
        </h2>
      )}
    </>
  );
};

export default Board;
